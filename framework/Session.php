<?php

namespace Framework;

use Exception;

/**
 *
 * Class Session
 *
 * @package Framework
 *
 */
class Session
{

    /**
     *
     * Session constructor
     *
     */
    public function __construct()
    {

        session_start();

    }

    /**
     *
     * Détruit la session actuelle
     *
     */
    public function destroySession()
    {

        session_destroy();

    }

    /**
     *
     * Ajoute un attribut à la session
     *
     * @param string $name Nom de l'attribut
     * @param mixed $value Valeur de l'attribut
     *
     */
    public function setAttribute($name, $value)
    {

        $_SESSION[$name] = $value;

    }

    /**
     *
     * Renvoie la valeur de l'attribut demandé
     *
     * @param string $name Nom de l'attribut
     *
     * @return string|array Valeur de l'attribut
     *
     * @throws Exception Si l'attribut n'existe pas dans la session
     *
     */
    public function getAttribute($name)
    {

        $retour = null;

        if ($this->isAttribute($name)) {
            $retour = $_SESSION[$name];
        }

        return $retour;

    }

    /**
     *
     * Renvoie vrai si l'attribut existe dans la session
     *
     * @param string $name Nom de l'attribut
     * @return bool Vrai si l'attribut existe et sa valeur n'est pas vide
     *
     */
    public function isAttribute($name)
    {

        return isset($_SESSION[$name]);

    }

}