<?php

namespace Framework;

use Exception;
use PDO;
use PDOStatement;

/**
 *
 * Abstract Class Model
 *
 * @package Framework
 *
 */
abstract class Model
{

    /**
     *
     * Constantes
     *
     */
    /*
    const DSN = "mysql:host=localhost;dbname=bougies;charset=utf8";
    const LOGIN = "root";
    const PASSWORD = "";
    */

    /**
     *
     * @var PDO bdd objet PDO de connexion à la BDD
     *
     */
    private static $db;

    /**
     *
     * Exécute une requête SQL
     *
     * @param string $sql Requête SQL
     * @param array $parameters Paramètres de la requête
     *
     * @return PDOStatement Résultats de la requête
     *
     * @throws Exception
     *
     */
    protected function executeQuery($sql, $parameters = null)
    {

        if ($parameters == null) {
            $result = self::dbConnect()->query($sql);
        } else {
            $result = self::dbConnect()->prepare($sql);
            $result->execute($parameters);
        }

        return $result;

    }

    /**
     *
     * Renvoie un objet de connexion à la BDD en initialisant la connexion au besoin
     *
     * @return PDO Objet PDO de connexion à la BDD
     *
     * @throws Exception
     *
     */
    private static function dbConnect()
    {

        if (self::$db === null) {

            $dsn = Config::get('DNS_DB');
            $login = Config::get('LOGIN_DB');
            $password = Config::get('PASSWORD_DB');

            try {
                self::$db = new PDO($dsn, $login, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            } catch (Exception $e) {
                throw new Exception("Connexion à la BDD échoué : " . $e->getMessage());
            }

        }

        return self::$db;

    }

    /**
     * 
     * Permet de se deconnecter à la BDD
     * 
     */
    public static function dbDisconnect()
    {

        self::$db = null;

    }

}