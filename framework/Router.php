<?php

namespace Framework;

use Exception;

/**
 *
 * Class Router
 *
 * @package Framework
 *
 */
class Router
{

    /**
     *
     * Permet d'executer une route et donc de lancer le code
     *
     */
    public function routerRequest()
    {

        try {
            $request = new Request(array_merge($_GET, $_POST));
            $controller = $this->createController($request);
            $action = $this->createAction($request);
            $controller->executeAction($action);
        } catch (Exception $e) {
            $this->generateError($e);
        }

    }

    /**
     *
     * Permet de générer le bon controller associer à la requête
     *
     * @param Request $request Objet requête afin de passer les détails d'appel tel que le controller viser, l'action visé...
     *
     * @return mixed|string Retourne le controller associer à la requête passer en paramètre
     *
     * @throws Exception
     *
     */
    private function createController(Request $request)
    {

        $controllerString = ucfirst(strtolower(Config::get('DEFAULT_CONTROLLER')));

        $nameAttributeController = Config::get('NAME_ATTRIBUTE_CONTROLLER');

        if ($request->isParameter($nameAttributeController)) {
            $controllerString = $request->getParameter($nameAttributeController);
            $controllerString = ucfirst(strtolower($controllerString));
        }

        $className = Config::get('PREFIX_NAME_CONTROLLER') . $controllerString;

        $fileController = 'controllers/' . $className . '.php';

        if (file_exists($fileController)) {

            require($fileController);

            try {
                $classNameWithNamespace = 'Framework\\Controller\\'.$className;
                $controller = new $classNameWithNamespace();
            } catch (Exception $e) {
                throw new Exception("Le controller $classNameWithNamespace n'est pas instenciable");
            }

            $controller->setRequest($request);

            return $controller;

        } else {

            throw new Exception("Le controller $controllerString est introuvable");

        }

    }

    /**
     *
     * Méthode qui nous permet d'acceder directement à partir d'une requête à la méthode à executer
     *
     * @param Request $request Est la requete qui demande le controller, la méthode précise à invoquer
     *
     * @return mixed|string On retourne le nom de la méthode du controller qui devra être executer
     *
     * @throws Exception
     *
     */
    private function createAction(Request $request)
    {

        $action = Config::get('DEFAULT_ACTION');

        $nameAttributeAction = Config::get('NAME_ATTRIBUTE_ACTION');

        if ($request->isParameter($nameAttributeAction)) {
            $action = $request->getParameter($nameAttributeAction);
        }

        return $action;

    }

    /**
     *
     * Nous permet de gérer les erreurs sur une page dédié
     *
     * @param Exception $e Nature de l'erreur
     *
     * @throws Exception
     *
     */
    private function generateError(Exception $e)
    {

        $view = new View("Erreur", 'error');

        $role = isset($_SESSION['role']) ? $_SESSION['role'] : 0;
        $login = isset($_SESSION['login']) ? $_SESSION['login'] : null;

        $view->generate(['error' => $e->getMessage(), 'role' => $role, 'login' => $login]);

    }

}