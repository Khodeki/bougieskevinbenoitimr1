<?php

namespace Framework;

/**
 *
 * Class Cookie
 *
 * @package Framework
 *
 */
class Cookie
{

    /**
     *
     * Permet de créer un cookie.
     *
     * @param $name string Le nom du cookie.
     *
     * @param $value mixed Le contenu du cookie, on évitera de stocker des informations sensibles.
     *
     * @param $expires int La date d’expiration du cookie sous forme d’un timestamp UNIX. En seconde !
     *
     * @param $path string Le chemin sur le serveur sur lequel le cookie sera disponible.
     *        Si la valeur est ‘/’, le cookie sera disponible sur l’ensemble du domaine.
     *        Si la valeur est ‘/cours/’, le cookie ne sera disponible que dans le répertoire /cours/ et dans tous les sous-répertoires qu’il contient.
     *
     * @param $domain string Indique le domaine ou le sous domaine pour lequel le cookie est disponible.
     *
     * @param $secure bool Uniquement être transmis à travers une connexion sécurisée HTTPS depuis le client.
     *
     * @param $httponly bool Doit être accessible que par le protocole HTTP.
     *        Pour que le cookie ne soit accessible que par le protocole http, on indiquera la valeur true. Cela permet d’interdire l’accès au cookie aux langages de scripts comme le JavaScript par exemple, pour se protéger potentiellement d’une attaque de type XSS.
     *
     */
    public static function set($name, $value, $expires = 86400, $path = '/', $domain = '/', $secure = true, $httponly = false)
    {

        setcookie($name, $value, $expires + time(), $path, $domain, $secure, $httponly);

    }

    /**
     *
     * Permet de récupérer le contenu du cookie représenter par un id
     *
     * @param $value string C'est l'id du cookie afin de pouvoir récupérer son contenu
     *
     * @return mixed Le contenu du cookie
     *
     */
    public static function get($value)
    {

        return $_COOKIE[$value];

    }

}