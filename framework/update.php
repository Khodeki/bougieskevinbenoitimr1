
<?php

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Pensez à clear des data afin d'éviter d'avoir des données
 *      qui se balade et qui ne sont pas bien cloisonner.
 *
 */
$allow = ['nom', 'comments'];
foreach ($_POST as $key => $val) {
    if (! in_array($allow, $key) ) {
        unset($_POST[$key]);
    }
}

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * $_SERVER : ce sont des valeurs renvoyées par le serveur. Elles
 *      sont nombreuses, et quelques-unes d'entre elles peuvent
 *      nous être d'une grande utilité.
 *
 * Par exemple : $_SERVER['REMOTE_ADDR'] nous donne l'adresse
 *      IP du client qui a demandé à voir la page.
 *
 * $_ENV : ce sont des variables d'environnement toujours
 *      données par le serveur.
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Inclure un système d'auto-load pour le chargement des files.
 *
 */
spl_autoload_register ( function ( $className ) {
    include_once($className.'.php');
});

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * La class Form est à revoir pour des questions d'optimisation et
 *      de faciliter d'utilisation.
 *
 * Nous avons encore beaucoup trop de code pour gérer les
 *      formulaires dans les controller.
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Automatisation des génération des view, avec :
 *      $controller/
 *              $action.php
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Notre système de cloisonnement de role sur un controller, nous pousse
 *      à créer des controller par action possible.
 *
 * Ce qui aurait été préférable, c'est un controller Bougie avec les
 *      méthodes : creation, edition, consulter
 *
 * Mais du coup c'est plus compliqué pour gérer les droit d'accès car,
 *      il aurait fallut vérifier pour chaque méthode dut à notre framework,
 *      ce qui aurait allourdit le code au niveau des méthodes de controller.
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Gérer un peu mieux les redirection suite à une faute d'accès
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Gérer le fait que si on précise un id et qu'il en est pas attendu, ca renvoie quand même vers la bonne page.
 *
 */

/**
 *
 * AXE D'AMELIORATION DU FRAMEWORK
 *
 * Afficher des erreu plus complexe quand l'environement de dev est activer, et en prod, maquiller les erreurs.
 *
 */

// role(SESSION) + login(SESSION) + messages(SESSION) + controller(GET) + id(GET) + action(GET)