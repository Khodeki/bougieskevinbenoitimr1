<?php

namespace Framework;

require_once ('framework/Role.php');

use Exception;

/**
 *
 * Abstract Class Controller
 *
 * @package Framework
 *
 */
abstract class Controller implements Role
{

    /**
     *
     * Constantes
     *
     */
    const PARAMETER_LOGIN = 'login'; // Le login est une variable de session
    const PARAMETER_ROLE = 'role'; // Le role est une variable de session
    const PARAMETER_MESSAGES = 'messages'; // Les messages d'erreurs son stocker dans le POST

    /**
     *
     * @var Request $request Est la requete associer au Controller
     *
     */
    protected $request;

    /**
     *
     * @var string $action Qui représente la méthode à executer
     *
     */
    private $action;

    /**
     *
     * @var int $role Représente le role nécessaire pour accéder au méthode du controller en question.
     *
     */
    public $role = self::ROLE_VIEWER;

    /**
     *
     * Setter de Request
     *
     * @param Request $request La requête à setter sur la classe Request
     *
     */
    public function setRequest(Request $request)
    {

        $this->request = $request;

    }

    /**
     *
     * Nous permet d'executer la méthode appeler par la requête
     *
     * @param $action string Représente le nom de la méthode qui va être appeler
     *
     * @throws Exception
     *
     */
    public function executeAction($action)
    {

        if (method_exists($this, $action)) {
            $this->action = $action;
            // Mise en place des notion de personne connecter ou pas
            if ($this->request->getSession()->getAttribute(self::PARAMETER_LOGIN) != null) {
                // Mise en place de la gestion du role
                if ($this->getRole() >= $this->role) {
                    $this->{$this->action}();
                } else {
                    $this->addFlash('danger', "Vous n'avez pas les droits suffisant.");
                    $this->redirect(Config::get('DEFAULT_CONTROLLER'));
                }
            } elseif ($this->request->getParameter(Config::get('NAME_ATTRIBUTE_CONTROLLER')) == Config::get('CONNECTION_CONTROLLER')) {
                $this->{$this->action}();
            } else {
                $this->redirect(Config::get('CONNECTION_CONTROLLER'));
            }
        } else {
            throw new Exception("L'action $action est non définie pour " . get_class($this));
        }

    }

    /**
     *
     * Méthode qui sera executer par défault
     *
     * C'est l'action pard default
     *
     * Le fais de la déclarer ici la pousse les classes enfant à l'implémenter
     *
     * @return mixed
     *
     */
    public abstract function index();

    /**
     *
     * Méthode qui permettra de générer une vue qui sera associer à l'action qui l'appel et de son controller par conséquence
     *
     * @param string $title Titre de la page
     * @param string $file Représente le path d'acces au fichier de la vue
     * @param array $dataView Est l'ensemble des données à intégrer à la vue
     *
     * @throws Exception
     *
     */
    protected function generateView($title, $file, $dataView = array())
    {

        $view = new View($title, $file);

        $nameAttributeController = Config::get('NAME_ATTRIBUTE_CONTROLLER');
        $controller = Config::get('DEFAULT_CONTROLLER');

        if ($this->request->isParameter($nameAttributeController)) {
            $controller = $this->request->getParameter($nameAttributeController);
        }

        $dataView[Config::get('NAME_ATTRIBUTE_CONTROLLER')] = $controller;
        $dataView[Config::get('NAME_ATTRIBUTE_ACTION')] = $this->action;
        $dataView[Config::get('NAME_ATTRIBUTE_ID')] = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        $dataView[self::PARAMETER_ROLE] = $this->getRole();
        $dataView[self::PARAMETER_LOGIN] = $this->request->getSession()->getAttribute(self::PARAMETER_LOGIN);
        $dataView[self::PARAMETER_MESSAGES] = $this->request->getSession()->getAttribute(self::PARAMETER_MESSAGES);

        $view->generate($dataView);

    }

//    /**
//     *
//     * Effectue une redirection vers un contrôleur et une action spécifiques
//     *
//     * @param string $controller Le controleur associer
//     * @param string $action C'est la méthode que l'on souhaite executer
//     * @param string $id Représente un id afin de faire une modification sur entité précise
//     * @param array $messages Représente un tableau de message à transmettre à la page suivante
//     *
//     * @throws Exception
//     *
//     */
//    protected function redirectBis($controller, $action = null, $id = null, $messages = [])
//    {
//
//        $rootWeb = Config::get('ROOT_WEB');
//
//        unset($_POST[self::PARAETER_MESSAGES]); // On efface qui reste par sécurite
//        if (count($messages) > 0) {
//            $_POST[self::PARAETER_MESSAGES] = $messages;
//        }
//
//        $url = $controller;
//        if ($action != null) {
//            $url .= '/' . $action;
//            if ($id != null) {
//                $url .= '/' . $id;
//            }
//        }
//
//        header('Location:' . $rootWeb . $url);
//
//    }

    /**
     *
     * Effectue une redirection vers un contrôleur et une action spécifiques
     *
     * @param string $url Url à rediriger
     *
     * @throws Exception
     *
     */
    protected function redirect($url)
    {

        $rootWeb = Config::get('ROOT_WEB');

        header('Location:' . $rootWeb . $url);

    }

    /**
     *
     * Permet de récupérer le role de l'utilisateur en cours
     *
     * @return int Le role de l'utilisateur en cours
     *
     * @throws Exception
     *
     */
    protected function getRole() {

        // Code pour gérer les histoires de rôles
        $role = self::ROLE_VIEWER;

        if ($this->request->getSession()->isAttribute(self::PARAMETER_ROLE)) {
            $role = $this->request->getSession()->getAttribute(self::PARAMETER_ROLE);
        } else {
            $this->request->getSession()->setAttribute(self::PARAMETER_ROLE, $role);
        }

        return $role;

    }

    /**
     *
     * Méthode qui nous permet d'ajouter des message flash lors d'une redirection
     *
     * @param $type string Le type de message à afficher
     * @param $message string Le contenue du message à afficher
     *
     * @throws Exception
     *
     */
    protected function addFlash($type , $message) {

        $this->request->getSession()->setAttribute(self::PARAMETER_MESSAGES, [$type => $message]);

    }

}