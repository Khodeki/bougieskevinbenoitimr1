<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelAuteur
 *
 * id_auteur / nom_auteur
 *
 * @package Framework\Model
 *
 */
class ModelAuteur extends Model
{

    public function getAllAuteur() {

        $sql = 'SELECT * FROM bougies.auteur';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $auteurs = $this->getAllAuteur();

        $tmp = [];
        foreach ($auteurs as $auteur) {
            $tmp[$auteur['id_auteur']] = $auteur['nom_auteur'];
        }

        return $tmp;

    }

    public function getAuteur($id_auteur) {

        $sql = 'SELECT * FROM bougies.auteur WHERE auteur.id_auteur=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_auteur]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addAuteur($nom_auteur) {

        $sql = 'INSERT INTO bougies.auteur(nom_auteur) VALUES(:name)';

        try {
            $status = $this->executeQuery($sql, ['name' => $nom_auteur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteAuteur($id_auteur) {

        $sql = 'DELETE FROM bougies.auteur WHERE auteur.id_auteur = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_auteur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editAuteur($id_auteur, $nom_auteur) {

        $sql = 'UPDATE bougies.auteur SET auteur.nom_auteur = :nom WHERE auteur.id_auteur = :id';

        try {
            $status = $this->executeQuery($sql, ['nom' => $nom_auteur, 'id' => $id_auteur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}