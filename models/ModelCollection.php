<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelCollection
 *
 * id_collection / nom_collection
 *
 * @package Framework\Model
 *
 */
class ModelCollection extends Model
{

    public function getAllCollection() {

        $sql = 'SELECT * FROM bougies.collection';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $collections = $this->getAllCollection();

        $tmp = [];
        foreach ($collections as $collection) {
            $tmp[$collection['id_collection']] = $collection['nom_collection'];
        }

        return $tmp;

    }

    public function getCollection($id_collection) {

        $sql = 'SELECT * FROM bougies.collection WHERE collection.id_collection=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_collection]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addCollection($nom_collection) {

        $sql = 'INSERT INTO bougies.collection(nom_collection) VALUES(:nom)';

        try {
            $status = $this->executeQuery($sql, ['nom' => $nom_collection]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteCollection($id_collection) {

        $sql = 'DELETE FROM bougies.collection WHERE collection.id_collection = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_collection]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editCollection($id_collection, $nom_collection) {

        $sql = 'UPDATE bougies.collection SET collection.nom_collection = :nom WHERE collection.id_collection = :id';

        try {
            $status = $this->executeQuery($sql, ['nom' => $nom_collection, 'id' => $id_collection]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}