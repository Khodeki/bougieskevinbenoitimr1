<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelLivre
 *
 * id_livre / titre / id_auteur
 *
 * @package Framework\Model
 *
 */
class ModelLivre extends Model
{

    public function getAllLivre() {

        $sql = 'SELECT * FROM bougies.livre';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $livres = $this->getAllLivre();

        $tmp = [];
        foreach ($livres as $livre) {
            $tmp[$livre['id_livre']] = $livre['titre'];
        }

        return $tmp;

    }

    public function getLivre($id_livre) {

        $sql = 'SELECT * FROM bougies.livre WHERE livre.id_livre=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_livre]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addLivre($titre, $id_auteur = null) {

        $sql = 'INSERT INTO bougies.livre(titre, id_auteur) VALUES(:titre, :auteur)';

        try {
            $status = $this->executeQuery($sql, ['titre' => $titre, 'auteur' => $id_auteur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteLivre($id_livre) {

        $sql = 'DELETE FROM bougies.livre WHERE livre.id_livre = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_livre]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editLivre($id_livre, $titre, $id_auteur = null) {

        $sql = 'UPDATE bougies.livre SET livre.titre = :titre, livre.id_auteur = :auteur WHERE livre.id_livre = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_livre, 'titre' => $titre, 'auteur' => $id_auteur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}