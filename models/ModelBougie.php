<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelBougie
 *
 * id_bougie / nom_bougie / id_livre / id_collection / statut_bougie
 *
 * @package Framework\Model
 *
 */
class ModelBougie extends Model
{

    public function getAllBougie() {

        $sql = 'SELECT * FROM bougies.bougie';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $bougies = $this->getAllBougie();

        $tmp = [];
        foreach ($bougies as $bougie) {
            $tmp[$bougie['id_bougie']] = $bougie['nom_bougie'];
        }

        return $tmp;

    }

    public function getBougie($id_bougie) {

        $sql = 'SELECT * FROM bougies.bougie WHERE bougie.id_bougie=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_bougie]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addBougie($nom_bougie, $statut_bougie, $id_livre = null, $id_collection = null) {

        $sql = 'INSERT INTO bougies.bougie(nom_bougie, statut_bougie, id_livre, id_collection) VALUES(:nom, :statut, :livre, :collection)';

        try {
            $status = $this->executeQuery($sql, ['nom' => $nom_bougie, 'statut' => $statut_bougie, 'livre' => $id_livre, 'collection' => $id_collection]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteBougie($id_bougie) {

        $sql = 'DELETE FROM bougies.bougie WHERE bougie.id_bougie = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_bougie]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editBougie($id_bougie, $nom_bougie, $statut_bougie, $id_livre = null, $id_collection = null) {

        $sql = 'UPDATE bougies.bougie SET bougie.nom_bougie = :nom, bougie.statut_bougie = :statut, bougie.id_livre = :livre, bougie.id_collection = :collection WHERE bougie.id_bougie = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_bougie, 'nom' => $nom_bougie, 'statut' => $statut_bougie, 'livre' => $id_livre, 'collection' => $id_collection]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}