<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelOdeur
 *
 * id_odeur / nom_odeur / statut_odeur
 *
 * @package Framework\Model
 *
 */
class ModelOdeur extends Model
{

    public function getAllOdeur() {

        $sql = 'SELECT * FROM bougies.odeur';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $odeurs = $this->getAllOdeur();

        $tmp = [];
        foreach ($odeurs as $odeur) {
            $tmp[$odeur['id_odeur']] = $odeur['nom_odeur'];
        }

        return $tmp;

    }

    public function getOdeur($id_odeur) {

        $sql = 'SELECT * FROM bougies.odeur WHERE odeur.id_odeur=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_odeur]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addOdeur($nom_odeur, $statut_odeur) {

        $sql = 'INSERT INTO bougies.odeur(nom_odeur, statut_odeur) VALUES(:nom, :statut)';

        try {
            $status = $this->executeQuery($sql, ['nom' => $nom_odeur, 'statut' => $statut_odeur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteOdeur($id_odeur) {

        $sql = 'DELETE FROM bougies.odeur WHERE odeur.id_odeur = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_odeur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editOdeur($id_odeur, $nom_odeur, $statut_odeur) {

        $sql = 'UPDATE bougies.odeur SET odeur.nom_odeur = :nom, odeur.statut_odeur = :statut WHERE odeur.id_odeur = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_odeur, "nom" => $nom_odeur, 'statut' => $statut_odeur]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}