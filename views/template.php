<!DOCTYPE html>

<html lang="fr">

    <head>

        <!-- Title -->
        <title><?= $titre ?></title>

        <!-- Base Root -->
        <base href="<?= $racineWeb ?>">

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Description when share -->
        <meta name="og:site_name" content="BougiesLTE">
        <meta name="og:title" content="BougiesLTE Project">
        <meta name="og:description" content="ENSSAT Lannion - Project PHP - IMR 1">
        <meta name="og:url" content="#">
        <meta name="og:image" content="#">

        <!-- General meta -->
        <meta name="author" content="Kévin HODÉ, Benoit SAVALLI">
        <meta name="copyright" content="©2020-Kévin-Benoit">
        <meta name="keywords" content="BougiesLTE">
        <meta name="description" content="ENSSAT Lannion - Project PHP - IMR 1">
        <meta name="theme-color" content="#367FA9">
        <meta name="robots" content="noindex, nofollow, noarchive">

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="assets/img/Bougie_icon.png">

        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="assets/css/_all-skins.min.css">

        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- Canonical -->
        <link rel="canonical" href="https://kevinhode.fr/">

    </head>

    <body class="hold-transition skin-blue sidebar-mini">

        <div class="wrapper">

            <?php include 'components/header.php' ?>

            <?php include 'components/nav.php' ?>

            <main class="content-wrapper">

                <?php include 'components/alert.php' ?>

                <?= $contenu ?>

            </main>

            <?php include 'components/footer.php' ?>

        </div>

        <!-- jQuery 3 -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="assets/js/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="assets/js/adminlte.min.js"></script>
        <!-- Sparkline -->
        <script src="assets/js/jquery.sparkline.min.js"></script>
        <!-- SlimScroll -->
        <script src="assets/js/jquery.slimscroll.min.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

        <script src="assets/js/execDataTable.js"></script>
        <!-- FontAwesome -->
        <script src="https://kit.fontawesome.com/5f0083c829.js" crossorigin="anonymous"></script>

    </body>

</html>