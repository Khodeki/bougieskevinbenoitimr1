<?php if ($login != null): ?>
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>B</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Bougies</b>LTE</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="https://i.pravatar.cc/160" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?= $login ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="https://i.pravatar.cc/160" class="img-circle" alt="User Image">
                                <p>
                                    <?= $login ?> - BougieLTE
                                    <small>By Kévin HODÉ and Benoit SAVALLI</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profil/modifier" class="btn btn-default btn-flat">Modifier</a>
                                </div>
                                <div class="pull-right">
                                    <a href="profil/deconnexion" class="btn btn-default btn-flat">Déconnexion</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
<?php endif; ?>