
<?php if (isset($messages)) : ?>

    <?php foreach ($messages as $key => $message) : ?>

        <div class="alert alert-<?= $key ?> alert-dismissible" style="margin: 0 15px;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>
            <?php if ($key == 'danger'): ?>
                <i class="icon fas fa-ban"></i> Danger
            <?php elseif ($key == 'warning') : ?>
                <i class="icon fas fa-exclamation-triangle"></i> Attention
            <?php elseif ($key == 'success') : ?>
                <i class="icon fas fa-check"></i> Réussite
            <?php elseif ($key == 'info') : ?>
                <i class="icon fas fa-info"></i> Information
            <?php endif; ?>
            </h5>
            <?= $message ?>
        </div>

    <?php endforeach; ?>

    <?php unset($_SESSION['messages']); ?>

<?php endif; ?>