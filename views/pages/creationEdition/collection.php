<br><br>
<section class="content-header">
    <h1>
        <?= ucfirst($controller) ?>
        <small><?= ucfirst($action) ?></small>
    </h1>
</section>
<br><br>
<section class="container">
    <?= $form ?>
</section>