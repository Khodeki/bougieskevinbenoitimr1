<?php

namespace Framework\Controller;

require_once ('controllers/ControllerPublic.php');

use Framework\Config;
use Framework\Controller;
use Framework\Model\ModelUser;

/**
 *
 * Class ControllerUtilisateur
 *
 * @package Framework\Controller
 *
 */
class ControllerUtilisateurs extends Controller
{

    private $user;

    public function __construct()
    {
        $this->role = self::ROLE_ADMIN;
        $this->user = new ModelUser();
    }

    public function index()
    {
        $this->redirect(Config::get('DEFAULT_ROOT'));
    }

    public function liste()
    {
        $allUsers = $this->user->getAllUser();
        $headFooterKey = ['Login', 'Role'];
        $users = [];
        $i = 0;
        foreach ($allUsers as $row) {
            $users[$i]['id'] = $row['id'];
            $users[$i]['login'] = $row['login'];
            $users[$i]['role'] = $row['role'];
            $i++;
        }
        $this->generateView("Liste", 'pages/utilisateurs/liste', ['users' => $users, 'keyArray' => $headFooterKey]);
    }

    public function suppression()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->user->deleteUser($id);
            if ($isGood) {
                $user = $this->user->getUserById($id);
                $currentLogin = $this->request->getSession()->getAttribute(self::PARAMETER_LOGIN);
                $this->addFlash('success', "Vous avez supprimé un utilisateur.");
                if ($currentLogin == $user['login']) {
                    $this->redirect('profil/deconnexion');
                } else {
                    $this->redirect('utilisateurs/liste');
                }
            } else {
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function augmenter()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $user = $this->user->getUserById($id);
            if ($user != null) {
                $isGood = $this->user->updateRoleUser($id, $user['role']+1);
                if ($isGood) {
                    $currentLogin = $this->request->getSession()->getAttribute(self::PARAMETER_LOGIN);
                    $this->addFlash('success', "Vous avez réussi a augementé les droits un utilisateur.");
                    if ($currentLogin == $user['login']) {
                        $this->redirect('profil/deconnexion');
                    } else {
                        $this->redirect('utilisateurs/liste');
                    }
                } else {
                    $this->redirect(Config::get('DEFAULT_ROOT'));
                }
            } else {
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function diminuer()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $user = $this->user->getUserById($id);
            if ($user != null) {
                $isGood = $this->user->updateRoleUser($id, $user['role']-1);
                if ($isGood) {
                    $currentLogin = $this->request->getSession()->getAttribute(self::PARAMETER_LOGIN);
                    $this->addFlash('success', "Vous avez réussi a diminué les droits un utilisateur.");
                    if ($currentLogin == $user['login']) {
                        $this->redirect('profil/deconnexion');
                    } else {
                        $this->redirect('utilisateurs/liste');
                    }
                } else {
                    $this->redirect(Config::get('DEFAULT_ROOT'));
                }
            } else {
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

}