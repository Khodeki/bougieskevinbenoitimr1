<?php

namespace Framework\Controller;

require_once ('models/ModelUser.php');
require_once ('forms/FormUserLogin.php');
require_once ('forms/FormUserNew.php');

use Framework\Config;
use Framework\Controller;
use Framework\Form\FormUserLogin;
use Framework\Form\FormUserNew;
use Framework\Model\ModelUser;

/**
 *
 * Class ControllerProfil
 *
 * Important ce controller est accéssisble sous-aucune condition !
 *
 * @package Framework\Controller
 *
 */
class ControllerPublic extends Controller
{

    private $user;

    public function __construct()
    {
        $this->user = new ModelUser();
    }

    public function index()
    {
        $this->connexion();
    }

    public function connexion()
    {
        $form = new FormUserLogin();
        $htmlForm = $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                list($isConnected, $role) = $this->user->userConnection($data['userLogin'], $data['userPassword']);
                if ($isConnected) {
                    $this->request->getSession()->setAttribute(self::PARAMETER_ROLE, $role);
                    $this->request->getSession()->setAttribute(self::PARAMETER_LOGIN, $data['userLogin']);
                    $this->addFlash('success', "Connexion réussite !");
                    $this->redirect(Config::get('DEFAULT_ROOT'));
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Login ou mot de passe incorrect.");
                }
            } else {
                //Pre remplir le form avec les données saisies et les messages d'erreur
                $htmlForm = $form->buildHTML($data, $message);
            }
        } else {
            //Generer le formulaire vide
            $htmlForm = $form->buildHTML();
        }
        $this->generateView("Connexion", 'pages/public/connexion', ['form' => $htmlForm]);
    }

    public function creation()
    {
        $form = new FormUserNew();
        $htmlForm = $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                $isGood = $this->user->addUser($data['userLogin'], $data['userPassword'], 0);
                if ($isGood) {
                    $this->request->getSession()->setAttribute(self::PARAMETER_ROLE, 0);
                    $this->request->getSession()->setAttribute(self::PARAMETER_LOGIN, $data['userLogin']);
                    $this->addFlash('success', "Bienvenu ! Vous venez de créer un compte sur BougieLTE.");
                    $this->redirect(Config::get('DEFAULT_ROOT'));
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Problème lors de la création de votre compte.");
                }
            } else {
                //Pre remplir le form avec les données saisies et les messages d'erreur
                $htmlForm = $form->buildHTML($data, $message);
            }
        } else {
            //Generer le formulaire vide
            $htmlForm = $form->buildHTML();
        }
        $this->generateView("Création d'un compte", 'pages/public/creation', ['form' => $htmlForm]);
    }

}