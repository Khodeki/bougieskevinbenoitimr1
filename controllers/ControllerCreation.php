<?php

namespace Framework\Controller;

require_once ('controllers/ControllerModel.php');
require_once ('forms/FormBougie.php');
require_once ('forms/FormAuteur.php');
require_once ('forms/FormCollection.php');
require_once ('forms/FormLivre.php');
require_once ('forms/FormOdeur.php');
require_once ('forms/FormEvent.php');
require_once ('forms/FormRecette.php');

use Framework\Form\FormAuteur;
use Framework\Form\FormBougie;
use Framework\Form\FormCollection;
use Framework\Form\FormEvent;
use Framework\Form\FormLivre;
use Framework\Form\FormOdeur;
use Framework\Form\FormRecette;

/**
 *
 * Class ControllerCreation
 *
 * @package Framework\Controller
 *
 */
class ControllerCreation extends ControllerModel
{

    const LABEL_CREER = "Créer";
    const LABEL_CREATION = "Création";

    public function bougie()
    {
        $entity = 'bougie';
        $form = new FormBougie('creation/'.$entity, self::LABEL_CREER);
        $htmlForm = $form->generateAllFields([
            'bougieCollection' => $this->collection->generateAllValueForSelect(),
            'bougieLivre' => $this->livre->generateAllValueForSelect()
        ]);
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) { // On valide
                if ($this->livre->getLivre($data['bougieLivre']) != null) {
                    if ($this->collection->getCollection($data['bougieCollection']) != null) {
                        // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                        $isGood = $this->bougie->addBougie($data['bougieName'], $data['bougieStatus'], $data['bougieLivre'], $data['bougieCollection']);
                        if ($isGood) {
                            $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                            $this->redirect('consulter/'.$entity);
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                        }
                    } else {
                        $htmlForm = $form->buildHTML($data, [], "La collection sélectionner n'existe pas.");
                    }
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Le livre sélectionner n'existe pas.");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function collection()
    {
        $entity = 'collection';
        $form = new FormCollection('creation/'.$entity, self::LABEL_CREER);
        $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                $isGood = $this->collection->addCollection($data['collectionName']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function recette()
    {
        $entity = 'recette';
        $form = new FormRecette('creation/'.$entity, self::LABEL_CREER);
        $form->generateAllFields([
            'id_bougie' => $this->bougie->generateAllValueForSelect(),
            'id_odeur' => $this->odeur->generateAllValueForSelect()
        ]);
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                $isGood = $this->recette->addRecette($data['quantité'], $data['id_bougie'], $data['id_odeur']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function odeur()
    {
        $entity = 'odeur';
        $form = new FormOdeur('creation/'.$entity, self::LABEL_CREER);
        $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                $isGood = $this->odeur->addOdeur($data['nom_odeur'], $data['statut_odeur']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function event()
    {
        $entity = 'event';
        $form = new FormEvent('creation/'.$entity, self::LABEL_CREER);
        $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                $isGood = $this->event->addEvent($data['eventName']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function livre()
    {
        $entity = 'livre';
        $form = new FormLivre('creation/'.$entity, self::LABEL_CREER);
        $auteurs = $this->auteur->generateAllValueForSelect();
        $form->generateAllFields(['id_auteur' => $auteurs]);
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                $isGood = $this->livre->addLivre($data['titre'], $data['id_auteur']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            //Generer le formulaire vide
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

    public function auteur() {
        $entity = 'auteur';
        $form = new FormAuteur('creation/'.$entity, self::LABEL_CREER);
        $form->generateAllFields();
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                // En vue de la demande du client il ne semble pas avoir de contrainte de duplication concernant le champ nom
                $isGood = $this->auteur->addAuteur($data['auteurName']);
                if ($isGood) {
                    $this->addFlash('success', "Création réussite : ".$entity." a été créer.");
                    $this->redirect('consulter/'.$entity);
                    die(); // Fix flash printing
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de la création de $entity");
                }
            } else {
                $htmlForm = $form->buildHTML($data, $message, "Le formulaire n'est pas valide.");
            }
        } else {
            $htmlForm = $form->buildHTML();
        }
        $this->generateView(self::LABEL_CREATION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
    }

}