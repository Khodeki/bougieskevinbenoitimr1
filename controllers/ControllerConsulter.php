<?php

namespace Framework\Controller;

require_once ('controllers/ControllerModel.php');

/**
 *
 * Class ControllerConsulter
 *
 * @package Framework\Controller
 *
 */
class ControllerConsulter extends ControllerModel
{

    public function accueil()
    {

        $stat = [
            "auteur" => [
                'nb' => count($this->auteur->getAllAuteur()),
                'icon' => 'fas fa-pen',
                'color' => 'fuchsia'
            ],
            "bougie" => [
                "nb" => count($this->bougie->getAllBougie()),
                "icon" => "fas fa-birthday-cake",
                "color" => "aqua"
            ],
            "collection" => [
                "nb" => count($this->collection->getAllCollection()),
                "icon" => "fas fa-bars",
                "color" => "red"
            ],
            "event" => [
                'nb' => count($this->event->getAllEvent()),
                'icon' => 'fas fa-calendar-alt',
                'color' => 'green'
            ],
            "livre" => [
                'nb' => count($this->livre->getAllLivre()),
                'icon' => 'fas fa-book',
                'color' => 'yellow'
            ],
            "odeur" => [
                'nb' => count($this->odeur->getAllOdeur()),
                'icon' => 'fas fa-wine-glass',
                'color' => 'blue'
            ],
            "recette" => [
                'nb' => count($this->recette->getAllRecette()),
                'icon' => 'fas fa-cubes',
                'color' => 'purple'
            ],
            "utilisateur" => [
                'nb' => count($this->user->getAllUser()),
                'icon' => 'fas fa-users',
                'color' => 'teal'
            ]
        ];

        $this->generateView('Accueil', 'pages/consulter/accueil', ['data' => $stat]);
        
    }

    private function getCollectionNameLivreAuteurName($id_livre, $id_collection)
    {
        $res = null;
        $livre = $this->livre->getLivre($id_livre);
        $collection = $this->collection->getCollection($id_collection);
        if ($livre != null && $collection != null) {
            $auteur = $this->auteur->getAuteur($livre['id_auteur']);
            if ($auteur != null) {
                $livreAuteur = $livre['titre'] . ' - ' . $auteur['nom_auteur'];
                $res = [
                    'collection' => $collection['nom_collection'],
                    'livre' => $livreAuteur
                ];
            }
        }
        return $res;
    }

    public function bougie()
    {
        $allElements = $this->bougie->getAllBougie();
        $headFooterKey = ["Nom", "Statut", "Livre", "Collection", "Event"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id_bougie'] = $row['id_bougie'];
            $elements[$i]['nom_bougie'] = $row['nom_bougie'];
            $elements[$i]['statut_bougie'] = $row['statut_bougie'];
            $dataCalc = $this->getCollectionNameLivreAuteurName($row['id_livre'], $row['id_collection']);
            $elements[$i]['collection'] = isset($dataCalc['collection']) ? $dataCalc['collection'] : '';
            $elements[$i]['livre'] = isset($dataCalc['livre']) ? $dataCalc['livre'] : '';
            $events = $this->event->getEventsBougie($row['id_bougie']);
            $nameEvents = [];
            foreach ($events as $tab) {
                $currentEvent = $this->event->getEvent($tab['id_event']);
                array_push($nameEvents, $currentEvent['name']);
            }
            $elements[$i]['event'] = count($nameEvents) > 0 ? join(' ; ', $nameEvents) : '';
            $i++;
        }
        $this->generateView("Bougie", 'pages/consulter/bougie', ['bougies' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function collection()
    {
        $allElements = $this->collection->getAllCollection();
        $headFooterKey = ["Nom"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id'] = $row['id_collection'];
            $elements[$i]['nom'] = $row['nom_collection'];
            $i++;
        }
        $this->generateView("Collection", 'pages/consulter/collection', ['collections' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function recette()
    {
        $allElements = $this->recette->getAllRecette();
        $headFooterKey = ["Bougie", "Odeur", "Quantité"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id'] = $row['id_recette'];
            $bougie = $this->bougie->getBougie($row['id_bougie']);
            $elements[$i]['bougie'] = isset($bougie) ? $bougie['nom_bougie'] : '';
            $odeur = $this->odeur->getOdeur($row['id_odeur']);
            $elements[$i]['odeur'] = isset($odeur) ? $odeur['nom_odeur'] : '';
            $elements[$i]['quantite'] = $row['quantité'];
            $i++;
        }
        $this->generateView("Recette", 'pages/consulter/recette', ['recettes' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function odeur()
    {
        $allElements = $this->odeur->getAllOdeur();
        $headFooterKey = ["Nom", "Statut"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id'] = $row['id_odeur'];
            $elements[$i]['nom'] = $row['nom_odeur'];
            $elements[$i]['statut'] = $row['statut_odeur'];
            $i++;
        }
        $this->generateView("Odeur", 'pages/consulter/odeur', ['odeurs' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function event()
    {
        $allElements = $this->event->getAllEvent();
        $headFooterKey = ["Nom", "Nbr Bougie"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id'] = $row['id'];
            $elements[$i]['nom'] = $row['name'];
            $elements[$i]['nbrBougie'] = count($this->event->getEvents($row['id']));
            $i++;
        }
        $this->generateView("Event", 'pages/consulter/event', ['envents' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function livre()
    {
        $allElements = $this->livre->getAllLivre();
        $headFooterKey = ["Nom livre", "Nom auteur"];
        $elements = [];
        $i = 0;
        foreach ($allElements as $row) {
            $elements[$i]['id'] = $row['id_livre'];
            $elements[$i]['nom'] = $row['titre'];
            $auteur = $this->auteur->getAuteur($row['id_auteur']);
            $elements[$i]['id_auteur'] = isset($auteur) ? $auteur['nom_auteur'] : '';
            $i++;
        }
        $this->generateView("Livre", 'pages/consulter/livre', ['livres' => $elements, 'keyArray' => $headFooterKey]);
    }

    public function auteur()
    {
        $allAuteurs = $this->auteur->getAllAuteur();
        $headFooterKey = ["Nom"];
        $elements = [];
        $i = 0;
        foreach ($allAuteurs as $row) {
            $elements[$i]['id'] = $row['id_auteur'];
            $elements[$i]['nom'] = $row['nom_auteur'];
            $i++;
        }
        $this->generateView("Auteur", 'pages/consulter/auteur', ['auteurs' => $elements, 'keyArray' => $headFooterKey]);
    }

}