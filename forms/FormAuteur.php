<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormAuteur extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formAuteur', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'auteurName', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom de l'auteur"
        ]);

        return $this;

    }

}