<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormEvent extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formEvent', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'eventName', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom de l'évènement"
        ]);

        if (isset($option['eventBougie'])) {
            $this->addField(self::STR_TYPE_SELECT, 'eventBougie', "Bougie :", [
                'option' => $option['eventBougie'],
                'multiple' => 'multiple'],
                false
            );
        }

        return $this;

    }

}