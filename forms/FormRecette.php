<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormRecette extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formRecette', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_NUMBER, 'quantité', "Quantité :", [
            'min' => 0
        ])
            ->addField(self::STR_TYPE_SELECT, 'id_bougie', "Bougie :", [
                'option' => $option['id_bougie']
            ], false)
            ->addField(self::STR_TYPE_SELECT, 'id_odeur', "Odeur :", [
                'option' => $option['id_odeur']
            ], false);

        return $this;

    }

}