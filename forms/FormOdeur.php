<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormOdeur extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formOdeur', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'nom_odeur', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom de l'odeur"
        ])
            ->addField(self::STR_TYPE_SELECT, 'statut_odeur', "Status :", [
                'option' => [
                    'possess' => 'Possess',
                    'wish' => 'Wish',
                    'idea' => 'Idea'
                ]
            ], false);

        return $this;

    }

}