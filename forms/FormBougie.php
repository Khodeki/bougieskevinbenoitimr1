<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormBougie extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formBougie', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'bougieName', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom de la bougie"
        ])
            ->addField(self::STR_TYPE_SELECT, 'bougieCollection', "Collection :", [
                'option' => $option['bougieCollection']
         ], false)
            ->addField(self::STR_TYPE_SELECT, 'bougieLivre', "Livre :", [
                'option' => $option['bougieLivre']
            ], false)
            ->addField(self::STR_TYPE_SELECT, 'bougieStatus', "Status :", [
                'option' => [
                    'validée' => "Validée",
                    'neutre' => "Neutre",
                    'rejetée' => "Rejetée"
                ]
            ], false);

        return $this;

    }

}